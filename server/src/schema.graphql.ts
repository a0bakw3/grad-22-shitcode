import { gql } from "apollo-server"

export const typeDefs = gql`
  "Tells graphql server what to retrieve when we query it"
  type Query {
    getCoins: [Coin]
    getMarketData(coinId: String): [Market]
  }

  type Market {
    name: String
    base: String
    quote: String
    price_usd: Float
    price: Float
  }

  type Coin {
    id: String!
    name: String!
    symbol: String!
    rank: String!
  }
`