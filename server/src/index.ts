import { ApolloServer } from "apollo-server"
import { typeDefs } from "./schema.graphql"
import { CoinAPI } from "./dataSource/coinApi"
import { resolvers } from "./resolver"

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      coinAPI: new CoinAPI(),
    };
  },
})

server.listen().then(() => {
  console.log(`
    server running at https://studio.apollographql.com/dev
  `)
})