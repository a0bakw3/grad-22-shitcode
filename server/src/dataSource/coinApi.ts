import { RESTDataSource } from "apollo-datasource-rest"

export class CoinAPI extends RESTDataSource {
  constructor(){
    super()
    this.baseURL = 'https://api.coinlore.net/api'
  }
  getCoins(){
    return this.get('/tickers')
  }
  getMarketData(coinId: string){
    return this.get(`/coin/markets/?id=${coinId}`)
  }
}

