import { CoinAPI } from "./dataSource/coinApi"

type Context = {
  dataSources: {
    coinAPI: CoinAPI
  }
}
export const resolvers = {
  Query: {
    getCoins: async (_: unknown, __: unknown, {dataSources: {coinAPI}}: Context) => {
      try{
        const results = await coinAPI.getCoins()
        return results.data
      }catch(error){console.log(error)}
    },
    getMarketData: async (_: unknown, args: {coinId: string}, {dataSources: {coinAPI}}: Context) => {
      try{
        const results = await coinAPI.getMarketData(args.coinId)
        return results
      }catch(error){ console.log(error)}
    }
  }
}