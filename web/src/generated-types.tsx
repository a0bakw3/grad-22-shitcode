import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Coin = {
  __typename?: 'Coin';
  id: Scalars['String'];
  name: Scalars['String'];
  rank: Scalars['String'];
  symbol: Scalars['String'];
};

export type Market = {
  __typename?: 'Market';
  base?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  price_usd?: Maybe<Scalars['Float']>;
  quote?: Maybe<Scalars['String']>;
};

/** Tells graphql server what to retrieve when we query it */
export type Query = {
  __typename?: 'Query';
  getCoins?: Maybe<Array<Maybe<Coin>>>;
  getMarketData?: Maybe<Array<Maybe<Market>>>;
};


/** Tells graphql server what to retrieve when we query it */
export type QueryGetMarketDataArgs = {
  coinId?: InputMaybe<Scalars['String']>;
};

export type CoinsQueryVariables = Exact<{ [key: string]: never; }>;


export type CoinsQuery = { __typename?: 'Query', getCoins?: Array<{ __typename?: 'Coin', id: string, name: string, symbol: string, rank: string } | null> | null };

export type MarketQueryVariables = Exact<{
  coinId?: InputMaybe<Scalars['String']>;
}>;


export type MarketQuery = { __typename?: 'Query', getMarketData?: Array<{ __typename?: 'Market', name?: string | null, base?: string | null, quote?: string | null, price_usd?: number | null, price?: number | null } | null> | null };


export const CoinsDocument = gql`
    query Coins {
  getCoins {
    id
    name
    symbol
    rank
  }
}
    `;

/**
 * __useCoinsQuery__
 *
 * To run a query within a React component, call `useCoinsQuery` and pass it any options that fit your needs.
 * When your component renders, `useCoinsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCoinsQuery({
 *   variables: {
 *   },
 * });
 */
export function useCoinsQuery(baseOptions?: Apollo.QueryHookOptions<CoinsQuery, CoinsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CoinsQuery, CoinsQueryVariables>(CoinsDocument, options);
      }
export function useCoinsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CoinsQuery, CoinsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CoinsQuery, CoinsQueryVariables>(CoinsDocument, options);
        }
export type CoinsQueryHookResult = ReturnType<typeof useCoinsQuery>;
export type CoinsLazyQueryHookResult = ReturnType<typeof useCoinsLazyQuery>;
export type CoinsQueryResult = Apollo.QueryResult<CoinsQuery, CoinsQueryVariables>;
export const MarketDocument = gql`
    query Market($coinId: String) {
  getMarketData(coinId: $coinId) {
    name
    base
    quote
    price_usd
    price
  }
}
    `;

/**
 * __useMarketQuery__
 *
 * To run a query within a React component, call `useMarketQuery` and pass it any options that fit your needs.
 * When your component renders, `useMarketQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMarketQuery({
 *   variables: {
 *      coinId: // value for 'coinId'
 *   },
 * });
 */
export function useMarketQuery(baseOptions?: Apollo.QueryHookOptions<MarketQuery, MarketQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MarketQuery, MarketQueryVariables>(MarketDocument, options);
      }
export function useMarketLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MarketQuery, MarketQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MarketQuery, MarketQueryVariables>(MarketDocument, options);
        }
export type MarketQueryHookResult = ReturnType<typeof useMarketQuery>;
export type MarketLazyQueryHookResult = ReturnType<typeof useMarketLazyQuery>;
export type MarketQueryResult = Apollo.QueryResult<MarketQuery, MarketQueryVariables>;