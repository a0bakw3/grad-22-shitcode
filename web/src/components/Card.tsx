import { FC , useState} from "react"
import { useMarketQuery } from "../generated-types";
import {
  Heading,
  Box,
  Center,
  Text,
  Stack,
  Button,
  useColorModeValue,
  useDisclosure,
  ModalFooter,
  Modal,
  ModalOverlay,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  Flex,
  Stat,
  StatNumber,
  Divider
} from '@chakra-ui/react';
import { motion } from 'framer-motion'

import { Coin } from '../generated-types'

import { ActionButton } from "./ActionButton";


const variants = {
  hidden: { opacity: 0, x: 0, y: 20 },
  enter: { opacity: 1, x: 0, y: 0 },
  exit: { opacity: 0, x: 0, y: 20 },
}
type CoinProps = {
  coin: Coin
  onClicker?: () => void
}

export const Card: FC<CoinProps> = (props) => {
  const { onOpen, onClose, isOpen } = useDisclosure()
  
  const { data } = useMarketQuery({
    variables: {
      coinId: props.coin.id
    }
  })

  return (
    <motion.article
      initial="hidden" 
      animate="enter" 
      exit="exit" 
      variants={variants} 
      transition={{duration: 0.6, type: "easeInOut"}}
      style={{ position: "relative"}}
    >
      <Center m={4} mt={7}>
        <Box
          w="2xl"
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow={'2xl'}
          rounded={'md'}>
          <Box p={6}>
            <Stack spacing={0} align={'center'} mb={5}>
              <Heading fontSize={'2xl'} fontWeight={500} fontFamily={'body'}>
                {props.coin.name}
              </Heading>
              <Text color={'gray.500'}>{props.coin.symbol}</Text>
            </Stack>

            <Stack direction={'row'} justify={'center'} >
              <Stack spacing={0} align={'center'}>
                <Text fontWeight={600}>{props.coin.rank}</Text>
                <Text fontSize={'sm'} color={'gray.500'}>
                  Rank
                </Text>
              </Stack>
            </Stack>
            <Flex>
              <ActionButton
                onClickHandler={onOpen}
                name="More Info"
                bgColor="teal"
                mr={3}
              />
              <Button
                w={'full'}
                mt={8}
                bg='#f73b5b'
                color={'white'}
                rounded={'md'}
                _hover={{
                  transform: 'translateY(-2px)',
                  boxShadow: 'lg',
                }}
                onClick={props.onClicker}
              >
                remove
              </Button>
            </Flex>
            <Modal isOpen={isOpen} onClose={onClose}>
              <ModalOverlay />
                <ModalContent>
                  <ModalHeader>{props.coin.name}</ModalHeader>
                  <ModalCloseButton />
                  <ModalBody>
                    {data?.getMarketData?.map((market, id: number) => (
                      <Flex flexDirection="column" key={id}>
                        <Text fontSize="2xl">{market.name}</Text>
                        <Text>{market.quote}</Text>
                        <Flex>
                          <Stat>
                            <StatNumber>{market.price}</StatNumber>
                          </Stat>
                        </Flex>
                        <Divider />
                      </Flex>
                    ))}
                  </ModalBody>
                </ModalContent>
            </Modal>
          </Box>
        </Box>
      </Center>
    </motion.article>
  );
}