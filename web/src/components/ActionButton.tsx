import { FC } from 'react'
import { Button, useColorModeValue } from "@chakra-ui/react"

type ButtonProps = {
  onClickHandler?: () => void
  name: string
  mr?: number
  bgColor: "good" | "bad" | "teal"
}


export const ActionButton: FC<ButtonProps> = (props) => {

  return(
    <Button
      w={'full'}
      mt={8}
      mr={props.mr}
      bg={ props.bgColor !== 
        "bad" ? useColorModeValue('#151f21', 'gray.900') :
        '#f73b5b'
      }
      color={'white'}
      rounded={'md'}
      _hover={{
        transform: 'translateY(-2px)',
        boxShadow: 'lg',
      }}
      onClick={props.onClickHandler}
    >
      {props.name}
    </Button>
  )
}