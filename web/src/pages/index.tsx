import { useState, useEffect } from 'react'
import { Flex, Spinner } from '@chakra-ui/react'

import _, { isEqual } from 'lodash'

import { Coin, useCoinsQuery } from '../generated-types'
import { Card } from '../components/Card'
import Navbar from '../components/Navbar'

const Index = () => {
  const {data, loading} = useCoinsQuery()

  return(
    <>
      <Navbar />
      <Flex 
        flexDirection="row"
        wrap="wrap"
        mt={6}
      >
        {loading ? 
          <Flex alignItems="center"  justifyItems="center" w="100vw" h="100vh">
            <Spinner m="auto" size="xl"/> 
          </Flex>
        :
        data.getCoins.map((coin) => (
          <Card 
            key={coin.id}
            coin={coin}
          />
        ))}
      </Flex> 
    </>
  )
}

export default Index
